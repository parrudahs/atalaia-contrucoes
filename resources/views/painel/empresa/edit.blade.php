@extends('painel.master')

@section('links')
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('css/skins/_all-skins.min.css')  }}">

    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
@endsection

@section('content')
       <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Empresa
            <small>Editar Pagina</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Editar Empresa</li>
          </ol>
        </section>  

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <!-- form start -->
                <form role="form" method="POST" action="{{ route('empresa.update', ['id' => $empresa->id]) }}" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="box-body ull">
                    @include('painel.errors.alert-success')
                    @include('painel.errors.alert-errors')
                  	<div class="form-group col-md-8">
                      <label for="exampleInputFile">Image</label>
                      <input type="file" name="image" class="btn btn-primary btn-file" name="image" id="exampleInputFile">
                    </div>
                    <div class="form-group col-md-4 pull-right">
                      <label for="exampleInputFile">Imagem atual</label>
                      <img src="{{ asset('uploads/'.$empresa->image) }}" class="img-resposive img-thumbnail pull-right" width="300px">
                    </div>
                    <div class="form-group col-md-8">
                      <label for="exampleInputEmail1">Titulo</label>
                      <input type="text" value="{{ $empresa->titulo }}" name="titulo" class="form-control" id="exampleInputEmail1" placeholder="Nome">
                    </div>
                    <div class="form-group col-md-8">
                      <label for="exampleInputPassword1">Descricão</label>
                      <textarea class="form-control" id="textarea" name="descricao" rows="15" placeholder="Descricao">{{ $empresa->conteudo }}</textarea>
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer col-md-12">
                    <button type="submit" class="btn btn-primary">Editar</button>
                    <a href="{{ route('dashboard') }}" class="btn btn-default">voltar</a>
                  </div>
                </form>
              </div><!-- /.box -->
              </div>
            </div>
    			</section>
        </div>
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
@endsection

@section('scripts')
    <!-- jQuery 2.1.4 -->
    <script src="{{ asset('plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('plugins/fastclick/fastclick.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('js/app.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('js/demo.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
    <script>
      $(function () {
        //bootstrap WYSIHTML5 - text editor
        $("#textarea").wysihtml5();
      });
    </script>
@endsection