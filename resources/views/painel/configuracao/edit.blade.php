@extends('painel.master')

@section('links')
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('css/skins/_all-skins.min.css')  }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
@endsection

@section('content')
       <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Configuração
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Editar Servico</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <!-- form start -->
                <form role="form" method="POST" action="{{ route('config.update', ['id' => 1]) }}" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="box-body ull">
                    @include('painel.errors.alert-success')
                    @include('painel.errors.alert-errors')
                    <div class="form-group col-md-6">
                      <label for="exampleInputEmail1">Nome</label>
                      <input type="text" value="{{ $config->nome }}" name="nome" class="form-control" id="exampleInputEmail1" placeholder="Nome">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputEmail1">Email</label>
                      <input type="email" value="{{ $config->email }}" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputEmail1">Telefone</label>
                      <input type="text" value="{{ $config->telefone }}" name="telefone" class="form-control" id="exampleInputEmail1" placeholder="Tefefone">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputEmail1">Outro Telefone</label>
                      <input type="text" value="{{ $config->seg_telefone }}" name="seg_telefone" class="form-control" id="exampleInputEmail1" placeholder="Outro Telefone">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputEmail1">Endereço</label>
                      <input type="text" value="{{ $config->endereco }}" name="endereco" class="form-control" id="exampleInputEmail1" placeholder="Endereco">
                    </div>
                    <div class="form-group col-md-2">
                      <label for="exampleInputEmail1">Numero</label>
                      <input type="text" value="{{ $config->numero }}" name="numero" class="form-control" id="exampleInputEmail1" placeholder="Numero">
                    </div>
                    <div class="form-group col-md-4">
                      <label for="exampleInputEmail1">Bairro</label>
                      <input type="text" value="{{ $config->bairro }}" name="bairro" class="form-control" id="exampleInputEmail1" placeholder="Bairro">
                    </div>
                    <div class="form-group col-md-5">
                      <label for="exampleInputEmail1">Cidade</label>
                      <input type="text" value="{{ $config->cidade }}" name="cidade" class="form-control" id="exampleInputEmail1" placeholder="Cidade">
                    </div>
                    <div class="form-group col-md-5">
                      <label for="exampleInputEmail1">Estado</label>
                      <input type="text" value="{{ $config->estado }}" name="estado" class="form-control" id="exampleInputEmail1" placeholder="Estado">
                    </div>
                    <div class="form-group col-md-2">
                      <label for="exampleInputEmail1">Sigla</label>
                      <input type="text" value="{{ $config->sigla }}" name="sigla" class="form-control" id="exampleInputEmail1" placeholder="Sigla">
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer col-md-12">
                    <button type="submit" class="btn btn-primary">Editar</button>
                    <a href="{{ route('dashboard') }}" class="btn btn-default">voltar</a>
                  </div>
                </form>
              </div><!-- /.box -->
              </div>
            </div>
    			</section>
        </div>
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
@endsection

@section('scripts')
    <!-- jQuery 2.1.4 -->
    <script src="{{ asset('plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('plugins/fastclick/fastclick.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('js/app.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('js/demo.js') }}"></script>
@endsection