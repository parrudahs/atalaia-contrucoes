<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    @yield('links')
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="{{ route('dashboard') }}" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>LT</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Admin</b>LTE</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  @if(count(Auth::user()->image) == 0)
                  <img src="{{ asset('uploads/avatar.png') }}" class="user-image" alt="User Image">
                  @else
                  <img src="{{ asset('uploads/'.Auth::user()->image) }}" class="user-image" alt="User Image">
                  @endif
                  <span class="hidden-xs">{{ Auth::user()->name }}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="{{ asset('uploads/'.Auth::user()->image) }}" class="img-circle" alt="User Image">
                    <p>
                      {{ Auth::user()->name }} - Web Developer
                      <small>Member since Nov. 2012</small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-right">
                      <a href="{{ route('logout') }}" class="btn btn-danger btn-flat">Sair</a>
                    </div>
                     <div class="pull-left">
                      <a href="{{ route('admin.usuario')}}" class="btn btn-default btn-flat">Alterar Perfil</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="{{ asset('uploads/'.Auth::user()->image) }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>{{ Auth::user()->name }}</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active treeview">
              <a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> <span>Home</span></a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-server"></i>
                <span>Serviços</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ route('servicos.listar') }}"><i class="fa fa-list-alt"></i> Listar</a></li>
                <li><a href="{{ route('servicos.create') }}"><i class="fa fa-plus"></i> Novo</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-photo"></i>
                <span>Fotos</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ route('fotos.listar') }}"><i class="fa fa-list-alt"></i> Listar</a></li>
                <li><a href="{{ route('fotos.create') }}"><i class="fa fa-plus"></i> Novo</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-file-photo-o"></i>
                <span>Banner</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ route('banner.listar') }}"><i class="fa fa-list-alt"></i> Listar</a></li>
                <li><a href="{{ route('banner.create') }}"><i class="fa fa-plus"></i> Novo</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-file-text-o"></i>
                <span>Pagina Empresa</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ route('empresa') }}" target="__blank"><i class="fa fa-list-alt"></i> Visualizar</a></li>
                <li><a href="{{ route('empresa.edit', ['id' => 1]) }}"><i class="fa fa-edit"></i> Editar</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-wrench"></i>
                <span>Configurações</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ route('config.edit', ['id' => 1]) }}"><i class="fa fa-edit"></i> Editar</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="{{ route('home') }}" target="__blank"><i class="fa fa-share"></i> <span>Vizualizar Site</span></a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      @yield('content')
      @yield('scripts')

  </body>
</html>
