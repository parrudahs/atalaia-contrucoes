@extends('painel.master')

@section('links')
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('css/skins/_all-skins.min.css')  }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
@endsection

@section('content')
       <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Adiciona Fotos
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Adicionar Fotos</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <!-- form start -->
                <form role="form" method="POST" action="{{ route('fotos.store') }}" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="box-body">
                  @include('painel.errors.alert-success')
                  @include('painel.errors.alert-errors')
                    <div class="form-group">
                      <label for="exampleInputFile">Selecione o serviço</label>
                      <select class="form-control" name="servico">
                        @foreach($servicos as $s)
                        <option value="{{ $s->id }}">{{ $s->nome }}</option>
                        @endforeach
                      </select>
                    </div>
                  	<div class="form-group">
                      <label for="exampleInputFile">Escolha as imagens</label>
                      <input type="file" name="image[]" multiple="multiple" accept="image/*" class="btn btn-primary btn-file" name="image" id="exampleInputFile">
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Adicionar</button>
                    <a href="{{ route('fotos.listar') }}" class="btn btn-default">voltar</a>
                  </div>
                </form>
              </div><!-- /.box -->
              </div>
            </div>
    			</section>
        </div>
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
@endsection

@section('scripts')
    <!-- jQuery 2.1.4 -->
    <script src="{{ asset('plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('plugins/fastclick/fastclick.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('js/app.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('js/demo.js') }}"></script>
@endsection