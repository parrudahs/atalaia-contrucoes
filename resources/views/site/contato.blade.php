@extends('site.master')

@section('title', 'Contato')

@section('page')
	<!-- Close Nav -->
	<div class="container contato">
		<div class="col-md-6 contato-info">
			<h1 class="title">Informações para contato</h1>
			<div class="row">
				<div class="col-md-4">
					<h2 class="title-two"><i class="glyphicon glyphicon-home"></i> endereço</h2>
				</div>
				<div class="col-md-8">
					<p>{{ $config->endereco }}, Nº {{ $config->numero }}, {{ $config->bairro }}</p>
					<p>{{ $config->cidade }}-{{ $config->sigla }}</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<h2 class="title-two"><i class="glyphicon glyphicon-phone-alt"></i> Telefones</h2>
				</div>
				<div class="col-md-8">
					<p>{{ $config->telefone }}</p>
					<p>{{ $config->seg_telefone }}</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<h2 class="title-two"> <i class="glyphicon glyphicon-envelope"></i> E-mail</h2>
				</div>
				<div class="col-md-8">
					<p>{{ $config->email }}</p>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<h1 class="title">Formulário</h1>
			@include('painel.errors.alert-success')
            @include('painel.errors.alert-errors')
			<form action="{{ route('contato.enviar') }}" class="form-contato" method="POST">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group">
				  <label class="control-label" for="nome">Nome</label>
				  <input type="text" name="nome" value="{{ old('nome') }}" class="form-control" id="inputSuccess1" placeholder="Nome">
				</div>
				<div class="form-group">
				  <label class="control-label" for="Email">Email</label>
				  <input type="email" name="email" value="{{ old('email') }}" class="form-control" id="inputSuccess1" placeholder="Email">
				</div>
				<div class="form-group">
				  <label class="control-label" for="nome">Telefone</label>
				  <input type="text" name="telefone" value="{{ old('telefone') }}" class="form-control" id="inputSuccess1" placeholder="(xx) xxxx-xxxx">
				</div>
				<div class="form-group">
				  <label class="control-label" for="nome">Mensagem</label>
				  <textarea class="form-control" name="mensagem" rows="4" placeholder="mensagem">{{ old('mensagem') }}</textarea>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-success btn-lg">
					<input type="reset" class="btn btn-default btn-lg" value="Limpar Formulário">
				</div>
			</form>
		</div>
	</div>
@endsection