@extends('site.master')

@section('page')
<!-- Start Banner -->
	<div class="banner">
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
		  <div class="carousel-inner banner" role="listbox">
		  	@foreach($banner as $b)
		    <div class="item banner @if($b->id === $banner->first()->id) {{ 'active' }} @endif">
		      <img src="{{ asset('uploads/grande/'.$b->img)}}" alt="{{ $b->title }}" class="img-responsive center">
		      <div class="carousel-caption">
		        <h1>{{ $b->title }}</h1>
		      </div>
		    </div>
		    @endforeach
		  </div>
		  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
		    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
		    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		</div>
	</div>
	<!-- close banner -->
	<!-- Start Services -->
	<section class="services container-fluid">
		<div class="container">
			<h2>serviços</h2>
			@foreach($servicos as $s)
			<div class="col-md-3 box">
				<a href="{{ route('single', ['id' => $s->id ])}}">
					<div>
						<img src="{{ asset('uploads/grande/'.$s->image)}}" alt="{{ $s->nome }}" class="img-responsive" style="height:200px;">
						<h3>{{ $s->nome }}</h3>
					</div>
				</a>
			</div>
			@endforeach
		</div>
	</section>
	<!-- Close Services -->
@endsection
