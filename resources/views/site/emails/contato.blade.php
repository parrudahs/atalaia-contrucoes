Você recebeu uma mensagem da Atalaia Construções :
<p>
<span style="font-weight:bold;">Name:</span> {{ $nome }}
</p>
<p>
<span style="font-weight:bold;">E-mail:</span> {{ $email }}
</p>
<p>
<span style="font-weight:bold;">Telefone:</span> {{ $telefone }}
</p>
<p>
<span style="font-weight:bold;">Mensagem:</span> {{ $mensagem }}
</p>