@extends('site.master')

@section('title', 'Serviços')

@section('page')
	<div class="container page-services">
		<h1 class="title">serviços</h1>
		@foreach($servicos as $s)
		<div class="col-md-3 box">
			<img src="{{ asset('uploads/grande/'.$s->image) }}" alt="{{ $s->nome }}" class="img-responsive" style="height:200px;">
			<h3>{{ $s->nome }}</h3>
			<p>{{ strip_tags(str_limit($s->descricao, 80)) }}</p>
			<a href="{{ route('single',['id' => $s->id ]) }}" class="btn btn-default">saiba mais</a>
		</div>
		@endforeach
	</div>
@endsection
