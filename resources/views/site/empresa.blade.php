@extends('site.master')

@section('title', 'Empresa')

@section('page')
	<!-- Close Nav -->
	<div class="container empresa">
		<div class="col-md-8">
			<h1 class="title">{{ $empresa->titulo }}</h1>
			{!! $empresa->conteudo !!}
		</div>
		<div class="col-md-4">
			<img src="{{ asset('uploads/'.$empresa->image)}}" alt="" class="img-responsive img-rounded">
		</div>
	</div>
@endsection