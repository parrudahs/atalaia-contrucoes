@extends('site.master')

@section('title', 'Serviços')

@section('page')

<div class="container">
	<div class="col-md-6 single-page-services">
		<h1 class="title">{{ $servico->nome }}</h1>
		{!! $servico->descricao !!}
	</div>

	<div class="col-md-6">
		<div id="blueimp-gallery" class="blueimp-gallery" data-use-bootstrap-modal="false">>
	    <!-- The container for the modal slides -->
		    <div class="slides"></div>
		    <!-- Controls for the borderless lightbox -->
		    <h3 class="title"></h3>
		    <a class="prev">‹</a>
		    <a class="next">›</a>
		    <a class="close">×</a>
		    <a class="play-pause"></a>
		    <ol class="indicator"></ol>
		    <!-- The modal dialog, which will be used to wrap the lightbox content -->
		    <div class="modal fade">
		        <div class="modal-dialog">
		            <div class="modal-content">
		                <div class="modal-header">
		                    <button type="button" class="close" aria-hidden="true">&times;</button>
		                    <h4 class="modal-title"></h4>
		                </div>
		                <div class="modal-body next"></div>
		                <div class="modal-footer">
		                    <button type="button" class="btn btn-default pull-left prev">
		                        <i class="glyphicon glyphicon-chevron-left"></i>
		                        Previous
		                    </button>
		                    <button type="button" class="btn btn-primary next">
		                        Next
		                        <i class="glyphicon glyphicon-chevron-right"></i>
		                    </button>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>

		<div id="links">
			@foreach($fotos as $f)
		    <a href="{{ asset('uploads/grande/'.$f->nome) }}" title="{{ $servico->nome }}" data-gallery>
		        <img src="{{ asset('uploads/miniatura/'.$f->nome) }}" alt="Banana">
		    </a>
		    @endforeach
		</div>
	</div>

</div>

@endsection