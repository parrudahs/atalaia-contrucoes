@inject('config', 'App\Config')
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	{!! SEO::generate() !!}
	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
	<link rel="stylesheet" href="{{ asset('css/bootstrap-image-gallery.min.css') }}">
	<link rel="shortcut icon" href="{{ asset('imagens/favicon.ico')}}">
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
	<!-- Piwik -->
	<script type="text/javascript">
	  var _paq = _paq || [];
	  _paq.push(['trackPageView']);
	  _paq.push(['enableLinkTracking']);
	  (function() {
	    var u="http://cluster-piwik.locaweb.com.br/";
	    _paq.push(['setTrackerUrl', u+'piwik.php']);
	    _paq.push(['setSiteId', 433]);
	    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
	    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
	  })();
	</script>
	<!-- End Piwik Code -->
	<!-- start Header -->
	<header class="container header">
		<div class="row">
			<div class="col-md-2 col-xs-10">
				<a href="{{ route('home') }}">
					<img src="{{ asset('imagens/logo.png') }}" alt="logo marca" class="img-circle logo">
				</a>
			</div>
			<div class="col-md-5 col-md-offset-5 orcamento">
				<h2>Solicite Orçamento</h2>
				<div class="phone col-md-8 col-md-offset-2">
					<span><i class="glyphicon glyphicon-earphone"></i> {{ $config->find(1)->telefone }}</span>
				</div>
			</div>
		</div>
	</header>
	<!-- Close Header-->
	<!-- start Nav -->
	<nav class="navbar navbar-default menu">
	  <div class="container">
	  	<div class="col-md-9 col-md-offset-3">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li {{ Request::is('/') ? 'class="active"' : '' }}>
		        	<a href="{{route('home')}}">home</a>
		        </li>
		        <li><a href="{{ route('empresa') }}">a empresa</a></li>
		        <li><a href="{{ route('servicos')}}">serviços</a></li>
		        <li><a href="{{ route('contato')}}">contato</a></li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div>
	  </div><!-- /.container-fluid -->
	</nav>
	<!-- Close Nav -->

	@yield('page')

	<footer class="footer container-fluid">
		<div class="container text-center">
			<h2>{{ $config->find(1)->nome }}</h2>
			<span class="adress">{{ $config->find(1)->endereco}},Nº {{ $config->find(1)->numero }},
				{{ $config->find(1)->bairro }} - {{ $config->find(1)->cidade }}-{{ $config->find(1)->sigla }}</span><br>
			<span>Telefone:{{ $config->find(1)->telefone }}|E-mail: {{ $config->find(1)->email }}</span>
			<p>2015 Copyright Atalaia Construções e reformas Ltda</p>
		</div>
	</footer>
	<!-- Scripts Js -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap-image-gallery.min.js') }}"></script>
</body>
</html>