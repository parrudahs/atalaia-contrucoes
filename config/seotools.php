<?php

return [
    'meta'      => [
        /*
         * The default configurations to be used by the meta generator.
         */
        'defaults'       => [
            'title'       => "Atalaia Construções e Reformas", // set false to total remove
            'description' => 'A Atalaia Construções é uma empresa especializada na execução de reformas', // set false to total remove
            'separator'   => ' - ',
            'keywords'    => ['construções', 'reformas em gerais', 'fale conosco', 'orçamentos', 'serviços'],
        ],

        /*
         * Webmaster tags are always added.
         */
        'webmaster_tags' => [
            'google'    => 'google-site-verification=dAa2EmeEkCwbEf96TQ8zmKSVWHwiRBv5wumj1PvV9-M',
            'bing'      => null,
            'alexa'     => null,
            'pinterest' => null,
            'yandex'    => null
        ]
    ],
    'opengraph' => [
        /*
         * The default configurations to be used by the opengraph generator.
         */
        'defaults' => [
            'title'       => 'Atalaia Construções e Reformas', // set false to total remove
            'description' => 'A Atalaia Construções é uma empresa especializada na execução de reformas', // set false to total remove
            'url'         => false,
            'type'        => false,
            'site_name'   => false,
            'images'      => [],
        ]
    ],
    'twitter' => [
        /*
         * The default values to be used by the twitter cards generator.
         */
        'defaults' => [
          //'card'        => 'summary',
          //'site'        => '@LuizVinicius73',
        ]
    ]
];
