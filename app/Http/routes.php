<?php

/*************************************************
***************** ROTAS DO SITE ******************
*************************************************/

Route::get('/', ['uses' => 'ServicosController@paginaHome','as' => 'home']);

/** rotas da empresa */
Route::get('empresa', ['uses' => 'EmpresaController@index','as' => 'empresa']);

/** rotas do serviços */
Route::get('servicos', ['uses' => 'ServicosController@servicos','as' => 'servicos']);
Route::get('servico/{id}', ['uses'=> 'ServicosController@show', 'as' => 'single']);

/** rotas do contato */
Route::get('contato', ['uses' => 'ConfiguracaoController@index','as' => 'contato']);
Route::post('contato', ['uses' => 'ConfiguracaoController@contato', 'as' => 'contato.enviar']);


/*************************************************
***************** ROTAS DO PAINEL ****************
*************************************************/

Route::get('login', ['uses' => 'LoginController@index', 'as' => 'login']);
Route::post('login', ['uses' => 'LoginController@login', 'as' => 'entrar']);
Route::get('logout', ['uses' => 'LoginController@logout', 'as' => 'logout']);


Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){
	Route::get('/', ['as' => 'dashboard', function() {
		return view('painel.index');
	}]);

	/** Midleware do Usuario */
	Route::get('user', ['uses' => 'UserController@edit','as' => 'admin.usuario']);
	Route::post('user', ['uses' => 'UserController@update', 'as' => 'admin.alterar-user']);

	/** Midleware do servicos */
	Route::get('servicos', ['uses' => 'ServicosController@index', 'as' => 'servicos.listar']);
	Route::get('servicos-create', ['uses' => 'ServicosController@create', 'as' => 'servicos.create']);
	Route::post('servicos-create', ['uses' => 'ServicosController@store', 'as' => 'servicos.store']);
	Route::get('servicos-delete/{id}', ['uses' => 'ServicosController@destroy', 'as' => 'servicos.delete']);
	Route::get('servicos-edit/{id}', ['uses' => 'ServicosController@edit', 'as' => 'servico.edit']);
	Route::post('servicos-edit/{id}', ['uses' => 'ServicosController@update', 'as' => 'servico.update']);

	/** Middleware das fotos */
	Route::get('fotos', ['uses' => 'FotosController@index', 'as' => 'fotos.listar']);
	Route::get('fotos-create', ['uses' => 'FotosController@create', 'as' => 'fotos.create']);
	Route::post('fotos-create', ['uses' => 'FotosController@store', 'as' => 'fotos.store']);
	Route::get('fotos-delete/{id}', ['uses' => 'FotosController@destroy', 'as' => 'fotos.delete']);

	/** Middleware da Config */
	Route::get('configuracao/{id}', ['uses' => 'ConfiguracaoController@edit', 'as' => 'config.edit']);
	Route::post('configuracao/{id}', ['uses' => 'ConfiguracaoController@update', 'as' => 'config.update']);

	/** Middleware da Empresa */
	Route::get('empresa/{id}', ['uses' => 'EmpresaController@edit', 'as' => 'empresa.edit']);
	Route::post('empresa/{id}', ['uses' => 'EmpresaController@update', 'as' => 'empresa.update']);

	/** Middleware do banner */
	Route::get('banner-create', ['uses' => 'BannerController@create', 'as' => 'banner.create']);
	Route::post('banner-create', ['uses' => 'BannerController@store', 'as' => 'banner.store']);
	Route::get('banner', ['uses' => 'BannerController@index', 'as' => 'banner.listar']);
	Route::get('banner-delete/{id}', ['uses' => 'BannerController@destroy', 'as' => 'banner.destroy']);
});