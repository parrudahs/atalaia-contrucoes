<?php

namespace App\Http\Controllers;

use App\Servicos;
use App\Fotos;
use App\Banner;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Image, SEO;

class ServicosController extends Controller
{

    protected $servicos;
    protected $fotos;
    protected $banner;

    public function __construct(Servicos $servicos, Fotos $fotos, Banner $banner){
        $this->servicos = $servicos;
        $this->fotos    = $fotos;
        $this->banner   = $banner;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servicos = $this->servicos->all();
        return view('painel.servicos.listar')->with(compact('servicos'));
    }

    /**
     * [paginaHome description]
     * @return [type] [description]
     */
    public function paginaHome()
    {
        $servicos = $this->servicos->orderBy('id', 'desc')->get();
        $banner   = $this->banner->orderBy('id', 'desc')->take(4)->get();
        $isFirst = true;
        return view('site.index')->with(compact('servicos', 'banner', 'isFirst'));
    }

    /**
     * [servico description]
     * @return [type] [description]
     */
    public function servicos()
    {
        $servicos = $this->servicos->orderBy('id', 'desc')->get();
        return view('site.servicos')->with(compact('servicos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('painel.servicos.novo');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $val = Validator::make($request->all(), [
            'image'     => 'required|image',
            'nome'      => 'required|min:3',
            'descricao' => 'required|min:20|max:200'
        ]);

        if($val->fails())
            return redirect()->back()->withErrors($val)->withInput();

        if($request->file('image')->isValid()){
            $extensao   = $request->file('image')->getClientOriginalExtension();
            $fileName   = rand(1111,9999).'.'.$extensao;

            Image::make($request->file('image'))->resize(700,null,function ($constraint) {
                $constraint->aspectRatio();
            })->save('uploads/grande/'.$fileName);

            Image::make($request->file('image'))->resize(100,null,function ($constraint) {
                $constraint->aspectRatio();
            })->save('uploads/miniatura/'.$fileName);

            $servico = $this->servicos;
            $servico->image = $fileName;
            $servico->nome  = $request->get('nome');
            $servico->descricao = $request->get('descricao');
            $servico->save();
            $request->session()->flash('alert-success', 'Serviço cadastrado com sucesso');
        }

        return redirect()->route('servicos.listar');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $servico = $this->servicos->find($id);
        $fotos   = $this->fotos->where('id_servico', $id)->get();
        return view('site.single-services')->with(compact('servico', 'fotos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $servicos = $this->servicos->find($id);
        return view('painel.servicos.edit')->with(compact('servicos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $val = Validator::make($request->all(), [
            'image'     => 'image',
            'nome'      => 'required|min:3',
            'descricao' => 'required|min:20'
        ]);

        if($val->fails())
            return redirect()->back()->withErrors($val)->withInput();

        $servico = $this->servicos->find($id);
        $servico->nome = $request->get('nome');
        $servico->descricao = $request->get('descricao');

        if($request->file('image') != null){
            if($request->file('image')->isValid()){
                $extensao   = $request->file('image')->getClientOriginalExtension();
                $fileName   = rand(1111,9999).'.'.$extensao;

                Image::make($request->file('image'))->resize(400,null,function ($constraint) {
                    $constraint->aspectRatio();
                })->save('uploads/grande/'.$fileName);

                Image::make($request->file('image'))->resize(200,null,function ($constraint) {
                    $constraint->aspectRatio();
                })->save('uploads/miniatura/'.$fileName);

                unlink('uploads/grande/'.$servico->image);
                unlink('uploads/miniatura/'.$servico->image);
                $servico->image = $fileName;
                $servico->save();
                $request->session()->flash('alert-success', 'Serviço cadastrado com sucesso');
            }
        }else {
            $servico->save();
            $request->session()->flash('alert-success', 'Serviço cadastrado com sucesso');
        }

        return redirect()->route('servicos.listar');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = $this->servicos->find($id);
        unlink('uploads/grande/'.$service->image);
        unlink('uploads/miniatura/'.$service->image);

        $fotos = $this->fotos->where('id_servico', $id)->get();
        foreach ($fotos as $f) {
            unlink('uploads/grande/'.$f->nome);
            unlink('uploads/miniatura/'.$f->nome);
            $f->delete();
        }
        $service->delete();
        session()->flash('alert-success', 'O Serviço foi deletado com sucesso');
        return redirect()->back();
    }
}
