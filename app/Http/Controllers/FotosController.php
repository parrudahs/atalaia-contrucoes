<?php

namespace App\Http\Controllers;

use App\Servicos;
use App\Fotos;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Image;

class FotosController extends Controller
{

    protected $servicos;
    protected $fotos;

    public function __construct(Servicos $servicos, Fotos $fotos){
        $this->servicos = $servicos;
        $this->fotos   = $fotos;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fotos = $this->fotos->paginate(18);
        return view('painel.fotos.listar')->with(compact('fotos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $servicos = $this->servicos->all();
        return view('painel.fotos.novo')->with(compact('servicos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach($request->file('image') as $image){

            $rules = array('image' => 'required|image');
            $val = Validator::make(array('image' => $image), $rules);

            if($val->passes()){
                $extensao   = $image->getClientOriginalExtension();
                $fileName   = rand(1111,9999).'.'.$extensao;

                Image::make($image)->resize(800,null,function ($constraint) {
                    $constraint->aspectRatio();
                })->save('uploads/grande/'.$fileName);

                Image::make($image)->resize(100,null,function ($constraint) {
                    $constraint->aspectRatio();
                })->save('uploads/miniatura/'.$fileName);

                $this->fotos->create([
                    'nome' => $fileName,
                    'id_servico' => $request->get('servico')
                ]);

                $request->session()->flash('alert-success', 'As fotos foram adicionadas com sucesso');
            }else
                return redirect()->back()->withErrors($val)->withInput();
        }
        return redirect()->route('fotos.listar');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $foto = $this->fotos->find($id);
        unlink('uploads/miniatura/'.$foto->nome);
        unlink('uploads/grande/'.$foto->nome);
        $foto->delete();
        session()->flash('alert-success', 'A imagem foi deletada com sucesso');
        return redirect()->back();
    }
}
