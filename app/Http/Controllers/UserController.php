<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Image;

class UserController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $id = Auth::user()->id;
        $user = User::find($id);
        return view('painel.auth.altera')->with(compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $val = Validator::make($request->all(), [
            'image' => 'required|image',
            'nome'  => 'riquired',
            'password' => 'required|min:5|confirmed',
            'password_confirmation' => 'required|min:5'
        ]);

        if($val->fails())
            return redirect()->back()->withErrors($val)->withInput();

        $id   = Auth::user()->id;
        $user = User::find($id);

        $user->name     = $request->get('name');
        $user->password = bcrypt($request->get('password'));

        if($request->file('image')->isValid()){
            $extensao   = $request->file('image')->getClientOriginalExtension();
            $fileName   = rand(1111,9999).'.'.$extensao;

            unlink('uploads/'.$user->image);

            Image::make($request->file('image'))->resize(128,null,function ($constraint) {
                $constraint->aspectRatio();
            })->save('uploads/'.$fileName);

            $user->image = $fileName;
            $user->save();
            $request->session()->flash('alert-success', 'Perfil Alterado com Sucesso');
        }
        return redirect()->back();
    }
}
