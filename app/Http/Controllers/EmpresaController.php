<?php

namespace App\Http\Controllers;

use App\Empresa;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use SEO;

class EmpresaController extends Controller
{

    protected $empresa;

    /**
     * [__construct description]
     * @param Empresa $empresa [description]
     */
    public function __construct(Empresa $empresa)
    {
        $this->empresa = $empresa;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empresa = $this->empresa->find(1);
        SEO::setTitle($empresa->titulo);
        SEO::setDescription(str_limit($empresa->conteudo, $limit = 150, $end = '...') );
        return view('site.empresa')->with(compact('empresa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empresa = $this->empresa->find($id);
        return view('painel.empresa.edit')->with(compact('empresa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $val = Validator::make($request->all(), [
            'image'          => 'image|mimes:jpeg,bmp,png',
            'titulo'         => 'required|min:6',
            'descricao'      => 'required|min:100'
        ]);
        if($val->fails())
            return redirect()->back()->withErrors($val)->withInput();

        $empresa = $this->empresa->find($id);
        $empresa->titulo = $request->get('titulo');
        $empresa->conteudo = $request->get('descricao');

        if($request->hasFile('image')){
            $destino    = 'uploads';
            $extensao   = $request->file('image')->getClientOriginalExtension();
            $fileName   = rand(1111,9999).'.'.$extensao;
            $request->file('image')->move($destino, $fileName);
            $empresa->image = $fileName;
        }

        $empresa->save();
        $request->session()->flash('alert-success', 'Pagina alterada com sucesso');
        return redirect()->back();
    }
}
