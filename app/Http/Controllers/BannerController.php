<?php

namespace App\Http\Controllers;

use App\Banner;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator, Image;

class BannerController extends Controller
{

    protected $banner;

    /**
     * [__construct description]
     * @param Banner $banner [description]
     */
    public function __construct(Banner $banner)
    {
        $this->banner = $banner;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner = $this->banner->all();
        return view('painel.banner.listar')->with(compact('banner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('painel.banner.novo');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $val = Validator::make($request->all(), [
            'image' => 'required|image|mimes:jpeg,bmp,png',
            'titulo' => 'min:5'
        ]);

        if($val->fails())
            return redirect()->back()->withErrors($val)->withInput();

        $nova = $this->banner;
        $nova->title = $request->get('titulo');

        if($request->file('image')->isValid()){
            $extensao   = $request->file('image')->getClientOriginalExtension();
            $fileName   = rand(1111,9999).'.'.$extensao;
            Image::make($request->file('image'))->resize(1400,null,function ($constraint) {
                $constraint->aspectRatio();
            })->save('uploads/grande/'.$fileName);
            $nova->img  = $fileName;
            $nova->save();
            $request->session()->flash('alert-success', 'Banner adicionado com sucesso');
        }
        return redirect()->route('banner.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = $this->banner->find($id);
        unlink('uploads/grande/'.$banner->img);
        $banner->delete();
        session()->flash('alert-success', 'O Banner foi deletado com sucesso');
        return redirect()->back();
    }
}
