<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /**
     * mostra a pagina de login
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('painel.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $val = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);

        if($val->fails())
            return redirect()->back()->withErrors($val)->withInput();

        if(Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')]))
            return redirect()->route('dashboard');
        else
            return redirect()->back()->withErrors(['Email ou senha Inválida'])->withInput();
    }

    public function logout(){
        Auth::logout();
        return redirect('login');
    }
}
