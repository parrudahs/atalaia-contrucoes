<?php

namespace App\Http\Controllers;

use App\Config;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use SEO;

class ConfiguracaoController extends Controller
{

    protected $config;
    
    /**
     * [__construct description]
     * @param Config $config [description]
     */
    public function __construct(Config $config){
        $this->config = $config;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $config = $this->config->find(1);
        SEO::setTitle('Contato');
        SEO::setDescription('Entre em contato com a atalaia construções para fazer seu orçamento');
        return view('site.contato')->with(compact('config'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $config = $this->config->find($id);
        return view('painel.configuracao.edit')->with(compact('config'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $val = Validator::make($request->all(), [
            'nome'          => 'required',
            'email'         => 'required',
            'telefone'      => 'required',
            'seg_telefone'  => 'required',
            'endereco'      => 'required',
            'numero'        => 'required',
            'bairro'        => 'required',
            'cidade'        => 'required',
            'estado'        => 'required',
            'sigla'         => 'required'

        ]);

        if($val->fails())
            return redirect()->back()->withErrors($val)->withInput();

        $config = $this->config->find($id);
        $config->nome = $request->get('nome');
        $config->email = $request->get('email');
        $config->telefone = $request->get('telefone');
        $config->seg_telefone = $request->get('seg_telefone');
        $config->endereco = $request->get('endereco');
        $config->numero = $request->get('numero');
        $config->bairro = $request->get('bairro');
        $config->cidade = $request->get('cidade');
        $config->estado = $request->get('estado');
        $config->sigla = $request->get('sigla');
        $config->save();
        $request->session()->flash('alert-success', 'Configurações alteradas com sucesso');
        return redirect()->back();
    }

    /**
     * [contato description]
     * @return [type] [description]
     */
    public function contato(Request $request)
    {
        $val = Validator::make($request->all(), [
            'nome' => 'required',
            'email' => 'required',
            'telefone' => 'required',
            'mensagem' => 'required'
        ]);

        if($val->fails())
            return redirect()->back()->withErrors($val)->withInput();

        \Mail::send('site.emails.contato',[
            'nome' => $request->get('nome'),
            'email' => $request->get('email'),
            'telefone' => $request->get('telefone'),
            'mensagem' => $request->get('mensagem')
        ], function($mensage) use ($request){
            $mensage->from($request->get('email'));
            $mensage->to('atalaiaconstrucoes@outlook.com.br', 'Admin')->subject('Atalaia Construções - Contato');
        });

        $request->session()->flash('alert-success', 'Sua mensagem foi enviada com sucesso, em breve entraremos em contato ...');
        return redirect()->back();
    }
}
